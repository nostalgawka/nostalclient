package net.minecraft.nostalgawka.util;

import net.minecraft.client.Minecraft;

public class ClientUtil {
    public static final String VERSION = "1.0";
    public static final String BALANCE_REQUEST = "http://159.69.82.222:8000/api/graphql?query=query%20%7B%0A%20%20player(username%3A%22" + Minecraft.getMinecraft().thePlayer.username + "%22)%20%7B%0A%20%20%20%20PlayerBalance%0A%20%20%7D%0A%7D";
    public static final String SKIN_URL = "http://nostalgawka.xyz/skin/";
    public static final String CAPE_URL = "http://nostalgawka.xyz/cape/";
}
