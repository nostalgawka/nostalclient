package net.minecraft.nostalgawka.ui;

import net.minecraft.client.Minecraft;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.TimerTask;

public class UpdateInfoThread extends TimerTask {
    @Override
    public void run() {
        if(!Minecraft.username.equalsIgnoreCase("brak")) {
            try {
                float bal = Float.parseFloat(RequestHandler.getBalance(Minecraft.username));
                BigDecimal bd = new BigDecimal(bal).setScale(2, RoundingMode.HALF_UP);
                Minecraft.getMinecraft().playerBalance = String.valueOf(bd);
            } catch(NullPointerException e) {

            } catch (org.json.simple.parser.ParseException e) {
                e.printStackTrace();
            }
        }
    }
}
