package net.minecraft.nostalgawka.discord;

import club.minnced.discord.rpc.*;

public class DiscordHandler {
    public static DiscordRPC lib = DiscordRPC.INSTANCE;
    public static DiscordRichPresence presence = new DiscordRichPresence();
    public static void startPresence() {
        String applicationId = "660204969521315842";
        String steamId = "";
        DiscordEventHandlers handlers = new DiscordEventHandlers();
        lib.Discord_Initialize(applicationId, handlers, true, steamId);
        presence.startTimestamp = System.currentTimeMillis() / 1000; // epoch second
        presence.details = "W menu glownym.";
        lib.Discord_UpdatePresence(presence);
        DiscordHandler.lib.Discord_RunCallbacks();
    }
}
