package net.minecraft.nostalgawka.discord;

public class DiscordThread implements Runnable {
    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            DiscordHandler.lib.Discord_RunCallbacks();
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ignored) {}
        }
    }
}
