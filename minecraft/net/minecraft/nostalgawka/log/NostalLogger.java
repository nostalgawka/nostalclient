package net.minecraft.nostalgawka.log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class NostalLogger {
    File logs;
    FileOutputStream fos;
    private DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");

    public NostalLogger() {
        logs = new File(System.getenv("APPDATA") + "\\.nostalgawka\\nostalclient.log");
        if(!logs.exists()) {
            try {
                logs.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String getTime() {
        return dtf.format(LocalDateTime.now());
    }

    public void logInfo(String log) {
        System.out.println(getTime() + " [NOSTALCLIENT] " + log);
        if(logs.exists()) {
            try {
                fos = new FileOutputStream(logs, true);
                fos.write((getTime() + " [NOSTALCLIENT] " + log + "\n").getBytes());
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
