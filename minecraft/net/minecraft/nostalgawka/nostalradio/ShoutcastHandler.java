package net.minecraft.nostalgawka.nostalradio;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;
import net.minecraft.client.Minecraft;
import net.minecraft.src.GameSettings;

import javax.sound.sampled.*;
import java.io.InputStream;

public class ShoutcastHandler {

    private static Thread thread;
    private static Player player;
    public static boolean isRunning;
    static Line.Info source = Port.Info.HEADPHONE;
    static Line.Info sourceSpeakers = Port.Info.SPEAKER;

    public static void setStream(InputStream stream) {
        try {
            player = new Player(stream);
        } catch (JavaLayerException e) {
            e.printStackTrace();
        }
    }

    public static boolean isRunning() {
        return isRunning;
    }

    public static void start() {
        thread = new Thread(new Runnable() {
            public void run() {
                try {
                    if (AudioSystem.isLineSupported(source)) {
                        try {
                            Port outline = (Port) AudioSystem.getLine(source);
                            outline.open();
                            FloatControl volumeControl = (FloatControl) outline.getControl(FloatControl.Type.VOLUME);
                            float v = Minecraft.getMinecraft().gameSettings.musicVolume;
                            volumeControl.setValue(v);
                            player.play();
                        }
                        catch (LineUnavailableException ex) {
                            ex.printStackTrace();
                        }
                    }else {
                        Port outline = (Port) AudioSystem.getLine(sourceSpeakers);
                        outline.open();
                        FloatControl volumeControl = (FloatControl) outline.getControl(FloatControl.Type.VOLUME);
                        player.play();
                    }
                } catch (JavaLayerException | LineUnavailableException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        isRunning = true;
    }


    public static void stop() {
        if(isRunning()) {
            thread.interrupt();
            thread = null;
            if(player != null) {
                player.close();
                isRunning = false;
            }
        }
    }

}
