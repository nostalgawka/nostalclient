package net.minecraft.nostalgawka.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.minecraft.client.Minecraft;
import net.minecraft.nostalgawka.log.NostalLogger;

public class Config {

    public boolean nostalradio;

    public Config() {
        this.nostalradio = false;
    }
    private static Config instance;

    public static Config getInstance() {
        if (instance == null) {
            instance = fromDefaults();
        }
        return instance;
    }

    public static void load() {
            instance = fromFile(Minecraft.getMinecraft().config);
        if (instance == null) {
            instance = fromDefaults();
        }
    }

    private static Config fromDefaults() {
        Config config = new Config();
        return config;
    }

    public void toFile() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonConfig = gson.toJson(this);
        FileWriter writer;
        try {
            NostalLogger logger = new NostalLogger();
            writer = new FileWriter(Minecraft.getMinecraft().config);
            writer.write(jsonConfig);
            writer.flush();
            writer.close();
            logger.logInfo("Zapisano config pomyslnie");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Config fromFile(File configFile) {
        try {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(configFile)));
            return gson.fromJson(reader, Config.class);
        } catch (FileNotFoundException e) {
            return null;
        }
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(this);
    }
}
