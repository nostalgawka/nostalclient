package net.minecraft.src;

import net.minecraft.client.Minecraft;
import net.minecraft.nostalgawka.config.Config;
import net.minecraft.nostalgawka.nostalradio.ShoutcastHandler;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;

public class GuiIngameMenu extends GuiScreen
{
    /** Also counts the number of updates, not certain as to why yet. */
    private int updateCounter2 = 0;

    /** Counts the number of screen updates. */
    private int updateCounter = 0;

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */

    GuiButton btn;

    public void initGui()
    {
        this.updateCounter2 = 0;
        this.buttonList.clear();
        byte var1 = -16;
        this.buttonList.add(new GuiButton(1, this.width / 2 - 100, this.height / 4 + 120 + var1, StatCollector.translateToLocal("menu.returnToMenu")));

        if (!this.mc.isIntegratedServerRunning())
        {
            ((GuiButton)this.buttonList.get(0)).displayString = StatCollector.translateToLocal("menu.disconnect");
            this.buttonList.add(new GuiButton(2139, this.width / 2 + 2, this.height / 4 + 72 + var1, 98, 20, "Zmien skina"));
        }
        Config.load();
        if(Config.getInstance().nostalradio) {
            btn = new GuiButton(2138, this.width / 2 - 100, this.height / 2 + 12 + var1, 98, 20, "NostalRadio: ON");
        }else {
            btn = new GuiButton(2138, this.width / 2 - 100, this.height / 2 + 12 + var1, 98, 20, "NostalRadio: OFF");
        }
        this.buttonList.add(btn);
        this.buttonList.add(new GuiButton(4, this.width / 2 - 100, this.height / 4 + 24 + var1, StatCollector.translateToLocal("menu.returnToGame")));
        this.buttonList.add(new GuiButton(0, this.width / 2 - 100, this.height / 4 + 96 + var1, 98, 20, StatCollector.translateToLocal("menu.options")));
        GuiButton var3;
        this.buttonList.add(var3 = new GuiButton(7, this.width / 2 + 2, this.height / 4 + 96 + var1, 98, 20, StatCollector.translateToLocal("menu.shareToLan")));
        this.buttonList.add(new GuiButton(5, this.width / 2 - 100, this.height / 4 + 48 + var1, 98, 20, StatCollector.translateToLocal("gui.achievements")));
        this.buttonList.add(new GuiButton(6, this.width / 2 + 2, this.height / 4 + 48 + var1, 98, 20, StatCollector.translateToLocal("gui.stats")));
        var3.enabled = this.mc.isSingleplayer() && !this.mc.getIntegratedServer().getPublic();
    }

    public byte[] convert(File skinFile) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(ImageIO.read(skinFile).getSubimage(0, 0, 64, 32), "png", baos);
        return baos.toByteArray();
    }

    /**
     * Fired when a control is clicked. This is the equivalent of ActionListener.actionPerformed(ActionEvent e).
     */
    protected void actionPerformed(GuiButton par1GuiButton)
    {
        switch (par1GuiButton.id)
        {
            case 0:
                this.mc.displayGuiScreen(new GuiOptions(this, this.mc.gameSettings));
                break;

            case 1:
                par1GuiButton.enabled = false;
                this.mc.statFileWriter.readStat(StatList.leaveGameStat, 1);
                this.mc.theWorld.sendQuittingDisconnectingPacket();
                this.mc.loadWorld((WorldClient)null);
                this.mc.displayGuiScreen(new GuiMainMenu());

            case 2:
            case 3:
            default:
                break;

            case 4:
                this.mc.displayGuiScreen((GuiScreen)null);
                this.mc.setIngameFocus();
                this.mc.sndManager.resumeAllSounds();
                break;

            case 5:
                this.mc.displayGuiScreen(new GuiAchievements(this.mc.statFileWriter));
                break;

            case 6:
                this.mc.displayGuiScreen(new GuiStats(this, this.mc.statFileWriter));
                break;

            case 7:
                this.mc.displayGuiScreen(new GuiShareToLan(this));
                break;
            case 2138:
                Config.load();
                Config i = Config.getInstance();
                i.nostalradio = !i.nostalradio;
                Config.getInstance().toFile();
                if(i.nostalradio) {
                    btn.displayString = "NostalRadio: ON" ;
                    try {
                        ShoutcastHandler.setStream(new URL("https://listener1.mp3.tb-group.fm/tb.mp3").openStream());
                        ShoutcastHandler.start();
                        break;
                    } catch (IOException e) {
                        e.printStackTrace();
                        break;
                    }
                }else {
                    ShoutcastHandler.stop();
                    btn.displayString = "NostalRadio: OFF";
                    break;
                }
            case 2139:
                final JFileChooser chooser = new JFileChooser();
                final File[] skin = new File[1];
                final int[] returnVal = new int[1];
                EventQueue.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        FileNameExtensionFilter filter = new FileNameExtensionFilter("Skin w formacie .png", "png");
                        chooser.setFileFilter(filter);
                        returnVal[0] = chooser.showOpenDialog(null);
                        if(returnVal[0] == JFileChooser.APPROVE_OPTION) {
                            if(chooser.getSelectedFile().getAbsoluteFile().length() <= 8192) {
                                skin[0] = chooser.getSelectedFile();
                                byte[] bytesArray = new byte[(int) skin[0].length()];
                                FileInputStream fis = null;
                                try {
                                    fis = new FileInputStream(skin[0]);
                                    fis.read(bytesArray);
                                    fis.close();
                                    Minecraft.getMinecraft().getNetHandler().addToSendQueue(new Packet250CustomPayload("Nostalgawka|Skin", convert(skin[0])));
                                    System.out.println("Wyslano pakiet ze skinem");
                                    JOptionPane.showMessageDialog(Minecraft.getMinecraft().mcCanvas, "Zmieniono skina! Aby zobaczyc zmiany, pamietaj o zrestartowaniu Minecrafta.");
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }else {
                                JOptionPane.showMessageDialog(Minecraft.getMinecraft().mcCanvas, "Maksymalny rozmiar skórki to 8kb.");
                            }
                        }
                    }
                });
        }
    }

    /**
     * Called from the main game loop to update the screen.
     */
    public void updateScreen()
    {
        super.updateScreen();
        ++this.updateCounter;
    }

    /**
     * Draws the screen and all the components in it.
     */
    public void drawScreen(int par1, int par2, float par3)
    {
        this.drawDefaultBackground();
        this.drawCenteredString(this.fontRenderer, "Game menu", this.width / 2, 40, 16777215);
        super.drawScreen(par1, par2, par3);
    }
}
